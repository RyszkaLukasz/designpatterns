﻿using System;

namespace SimpleFactory
{
    public class PepperoniPizza : IPizza
    {
        public void Bake()
        {
            Console.WriteLine("Baking for 8 minuts");
        }

        public void Box()
        {
            Console.WriteLine("Packing the pizza into the box");
        }

        public void Cut()
        {
            Console.WriteLine("Cuting the pizza");
        }

        public void Prepare()
        {
            Console.WriteLine("Omato Sauce, Pepperoni and Herbes de Provence");
        }
    }
}