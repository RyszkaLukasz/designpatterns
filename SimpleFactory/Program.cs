﻿using System;
namespace SimpleFactory
{
    class Program
    {
        static void Main(string[] args)
        {
            PizzaStore store = new PizzaStore();

            Console.WriteLine("Veggie Pizza");
            store.OrderPizza(PizzaType.VeggiePizza);
            Console.WriteLine(string.Empty);

            Console.WriteLine("Pepperoni Pizza");
            store.OrderPizza(PizzaType.PepperoniPizza);
            Console.WriteLine(string.Empty);

            Console.ReadKey();
        }
    }
}
