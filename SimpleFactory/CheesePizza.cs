﻿using System;

namespace SimpleFactory
{
    public class CheesePizza : IPizza
    {
        public void Bake()
        {
            Console.WriteLine("Baking for 10 minuts");
        }

        public void Box()
        {
            Console.WriteLine("Packing the pizza into the box");
        }

        public void Cut()
        {
            Console.WriteLine("Cuting the pizza");
        }

        public void Prepare()
        {
            Console.WriteLine("Omato Sauce, Cheese and Herbes de Provence");
        }
    }
}
