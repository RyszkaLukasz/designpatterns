﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleFactory
{
    public class PizzaStore
    {
        private SimplePizzaFactory _pizzaFactory;

        public PizzaStore()
        {
            _pizzaFactory = new SimplePizzaFactory();
        }

        public IPizza OrderPizza(PizzaType type)
        {
            IPizza pizza = _pizzaFactory.Create(type);

            pizza.Prepare();
            pizza.Bake();
            pizza.Cut();
            pizza.Box();

            return pizza;
        }
    }
}
