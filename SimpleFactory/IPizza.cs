﻿namespace SimpleFactory
{
    public enum PizzaType
    {
        CheesePizza, ClamPizza, PepperoniPizza, VeggiePizza
    }

    public interface IPizza
    {
        void Prepare();

        void Bake();

        void Cut();

        void Box();
    }
}
