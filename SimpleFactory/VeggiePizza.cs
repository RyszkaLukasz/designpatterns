﻿using System;

namespace SimpleFactory
{
    public class VeggiePizza : IPizza
    {
        public void Bake()
        {
            Console.WriteLine("Baking for 11 minuts");
        }

        public void Box()
        {
            Console.WriteLine("Packing the pizza into the box");
        }

        public void Cut()
        {
            Console.WriteLine("Cuting the pizza");
        }

        public void Prepare()
        {
            Console.WriteLine("Omato Sauce, Onion, Tomatoes, Cheese and Herbes de Provence");
        }
    }
}