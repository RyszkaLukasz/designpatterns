﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace SimpleFactory
{
    public class SimplePizzaFactory
    {
        protected readonly IDictionary<PizzaType, Func<IPizza>> _pizzas = new ReadOnlyDictionary<PizzaType, Func<IPizza>>(new Dictionary<PizzaType, Func<IPizza>>()
        {
            { PizzaType.CheesePizza, () => new CheesePizza() },
            { PizzaType.ClamPizza, () => new ClamPizza() },
            { PizzaType.PepperoniPizza, () => new PepperoniPizza() },
            { PizzaType.VeggiePizza, () => new VeggiePizza() },
        });

        public IPizza Create(PizzaType type)
        {
            if (!_pizzas.ContainsKey(type))
                throw new Exception("Unknown pizza type!");
            return _pizzas[type]();
        }
    }
}
